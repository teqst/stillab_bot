import 'dotenv/config'
import express from 'express'
import TGBot from './entities/bot/bot.js'
import GitHookRouter from './routes/gitHook/index.js'
import bodyParser from 'body-parser'
import { Message } from 'node-telegram-bot-api'
import { botMessageHandler } from './entities/bot/index.js'
import {
  notifyAboutOpenedMergeRequests,
  setOpenedMrsTimer
} from './entities/notifier/mergeRequestNotifier.js'
import { setEstimateSessionNotifier } from './entities/notifier/estimateSessionNotifier.js'

const port = process.env.PORT ?? 3000

const botInstance = TGBot.getInstance()

const app = express()
app.use(
  bodyParser.urlencoded({
    extended: true
  })
)
app.use(bodyParser.json())

app.use('/githook', GitHookRouter)

function appListenCallback() {
  botInstance.on('message', (message: Message) =>
    botMessageHandler.apply(null, [message, botInstance])
  )

  setOpenedMrsTimer()
  setEstimateSessionNotifier()
}

app.listen(port, appListenCallback)
