import TelegramBot from 'node-telegram-bot-api'
import {
  MergeRequestEventObject,
  NoteEventObject,
  PipelineEventObject,
  PushEventObject
} from '../../controllers/gitHook/index.types.js'
import { onMergeRequestEvent } from './markdownHandlers/merge.js'
import { onPipelineEvent } from './markdownHandlers/pipeline.js'
import { onOpenedMergeRequestsEvent } from '../notifier/mergeRequestNotifier.js'
import { TelegramBotCommands } from './bot.types.js'
import { MergeRequestDBRow } from '../../models/mergeRequest/index.types.js'
import { CONFIG } from '../../config.js'
import { logMessage } from '../../utils/logUtil.js'

export default class TGBot {
  private static instance: TelegramBot

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  private constructor() {}

  static getInstance() {
    if (!TGBot.instance) {
      logMessage('Creating TelegramBot instance...')
      TGBot.instance = new TelegramBot(CONFIG.TOKEN, {
        polling: true
      })
      TGBot.instance.setMyCommands([
        {
          command: TelegramBotCommands.VERSION,
          description: 'Узнать версию бота'
        },
        {
          command: TelegramBotCommands.START,
          description: 'Подписаться на уведомления'
        },
        {
          command: TelegramBotCommands.OPENED_MRS,
          description: 'Узнать о незакрытых merge requests'
        }
        // { command: '/dailyJoke', description: 'Быстрый анекдот для рассказа на дейли'}
      ])
    }

    return TGBot.instance
  }

  static sendMergeRequestMessage(
    chatId: number,
    eventBody: MergeRequestEventObject
  ) {
    TGBot.getInstance().sendMessage(chatId, onMergeRequestEvent(eventBody), {
      parse_mode: 'Markdown'
    })
  }

  static sendOpenedMergeRequestMessage(
    chatId: number,
    eventBody: MergeRequestDBRow[],
    onMyOwn: boolean = false
  ) {
    TGBot.getInstance().sendMessage(
      chatId,
      onOpenedMergeRequestsEvent(eventBody, onMyOwn),
      {
        parse_mode: 'Markdown'
      }
    )
  }

  static sendEstimateSessionMessage(chatId: number, body: string) {
    TGBot.getInstance().sendMessage(chatId, body, {
      parse_mode: 'Markdown'
    })
  }

  static sendPipelineMessage(chatId: number, eventBody: PipelineEventObject) {
    if (!onPipelineEvent(eventBody).length) return

    TGBot.getInstance().sendMessage(chatId, onPipelineEvent(eventBody), {
      parse_mode: 'Markdown'
    })
  }

  static sendPushMessage(chatId: number, eventBody: PushEventObject) {
    TGBot.getInstance().sendMessage(
      chatId,
      `➡️ Пуш в *${eventBody.ref}*\n` +
        `*От:* *${eventBody.user_username}* _(${eventBody.user_name})_\n` +
        `*Проект:* [${eventBody.project.name}](${eventBody.project.web_url})\n` +
        `[Ссылка на ${eventBody.object_kind} 🔗](${eventBody.commits[0].url})`,
      {
        parse_mode: 'Markdown'
      }
    )
  }
  static sendNoteMessage(chatId: number, eventBody: NoteEventObject) {
    TGBot.getInstance().sendMessage(
      chatId,
      `📝 Комментарий: *${eventBody?.object_attributes?.description}*\n` +
        `*От:* *${eventBody.user.username}* _(${eventBody.user.name})_\n` +
        `*Проект:* [${eventBody.project.name}](${eventBody.project.web_url})\n` +
        `[Ссылка на ${eventBody?.object_kind} 🔗](${eventBody?.object_attributes?.url})`,
      {
        parse_mode: 'Markdown'
      }
    )
  }
}
