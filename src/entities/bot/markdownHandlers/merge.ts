import {
  MergeRequestActions,
  MergeRequestEventObject
} from '../../../controllers/gitHook/index.types.js'

export function onMergeRequestEvent(eventBody: MergeRequestEventObject) {
  if (
    eventBody.object_attributes.action === MergeRequestActions.approved ||
    eventBody.object_attributes.action === MergeRequestActions.approval
  ) {
    return approved(eventBody)
  }

  return merge(eventBody)
}

function approved(eventBody: MergeRequestEventObject): string {
  return (
    `✅ Запрос на слияние одобрен пользователем: *${eventBody.user.username}* _(${eventBody.user.name})_\n` +
    `_Из_ *${eventBody.object_attributes.source_branch}* => _в_ *${eventBody.object_attributes.target_branch}*\n` +
    `*Проект:* [${eventBody.project.name}](${eventBody.project.web_url})\n` +
    `[Ссылка на ${eventBody.object_kind} 🔗](${eventBody.object_attributes.url})`
  )
}

function merge(eventBody: MergeRequestEventObject): string {
  return (
    `📬 Запрос на слияние от: *${eventBody.user.username}* _(${eventBody.user.name})_\n` +
    `*Статус:* ${eventBody.object_attributes.merge_status}\n` +
    `*Состояние:* ${eventBody.object_attributes.state}\n` +
    `*Действие:* ${eventBody.object_attributes.action}\n` +
    `_Из_ *${eventBody.object_attributes.source_branch}* => _в_ *${eventBody.object_attributes.target_branch}*\n` +
    `*Проект:* [${eventBody.project.name}](${eventBody.project.web_url})\n` +
    `[Ссылка на ${eventBody.object_kind} 🔗](${eventBody.object_attributes.url})`
  )
}
