import {
  PipelineEventObject,
  PipelineStatuses
} from '../../../controllers/gitHook/index.types.js'

//TODO: Я не горжусь этим, но я сделал это. Надо бы переделать..
export function onPipelineEvent(eventBody: PipelineEventObject) {
  if (eventBody.object_attributes.detailed_status === PipelineStatuses.failed) {
    return failed(eventBody)
  } else if (
    eventBody.project.name === 'shared' &&
    eventBody.object_attributes.ref === 'develop'
  ) {
    return main(eventBody)
  }
  return ''
}

function failed(eventBody: PipelineEventObject): string {
  return (
    `🚀💥 Ошибка пайплайна *${eventBody.object_attributes.ref}*\n` +
    `*Статус:* ${eventBody.object_attributes.status}\n` +
    `*Подробности:* ${eventBody.object_attributes.detailed_status}\n` +
    `*Проект:* [${eventBody.project.name}](${eventBody.project.web_url})\n` +
    `[Ссылка на ${eventBody.object_kind} 🔗](${eventBody.project.web_url}/pipelines/${eventBody.object_attributes.id})`
  )
}

function main(eventBody: PipelineEventObject): string {
  return (
    `🏭 Пайплайн *${eventBody.object_attributes.ref}*\n` +
    `*Статус:* ${eventBody.object_attributes.status}\n` +
    `*Подробности:* ${eventBody.object_attributes.detailed_status}\n` +
    `*Проект:* [${eventBody.project.name}](${eventBody.project.web_url})\n` +
    `[Ссылка на ${eventBody.object_kind} 🔗](${eventBody.project.web_url}/pipelines/${eventBody.object_attributes.id})`
  )
}
