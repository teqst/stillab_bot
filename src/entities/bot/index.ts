import TelegramBot, { Message } from 'node-telegram-bot-api'
import { TelegramBotCommands } from './bot.types.js'
import { getChatIds, saveChatId } from '../../models/chats/index.js'
import { notifyAboutOpenedMergeRequests } from '../notifier/mergeRequestNotifier.js'
import { logMessage } from '../../utils/logUtil.js'

export async function botMessageHandler(
  message: Message,
  botInstance: TelegramBot
) {
  logMessage({ from: message.from?.username, date: message.date })
  const chatID = message.chat.id
  const data: Array<number> = []

  if (message.text === TelegramBotCommands.OPENED_MRS) {
    logMessage('-----------------Запрос открытых мерджей')
    await notifyAboutOpenedMergeRequests(chatID, true)
  }

  if (message.text === TelegramBotCommands.VERSION) {
    logMessage('-----------------Запрос версии')
    await botInstance.sendMessage(chatID, `Version ${process.env.APP_VERSION}`)
  }

  if (message.text === TelegramBotCommands.START) {
    logMessage('-----------------Присоединиться в чату')
    try {
      const allChatIds = await getChatIds()
      data.push(...allChatIds)
    } catch (err) {
      await saveChatId(chatID)
    }

    if (data.includes(chatID)) {
      await botInstance.sendMessage(
        chatID,
        `Привет, @${message.chat.username} ! Вы уже подписаны на оповещения.`
      )
    } else {
      await saveChatId(chatID)
      await botInstance.sendMessage(
        chatID,
        `Привет, @${message.chat.username} ! Вы добавлены в список оповещения.`
      )
    }

    logMessage('ChatIds:', data)
  }
}
