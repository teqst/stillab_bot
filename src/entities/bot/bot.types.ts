export enum TelegramBotCommands {
  START = '/start',
  VERSION = '/version',
  OPENED_MRS = '/opened_mrs'
}
