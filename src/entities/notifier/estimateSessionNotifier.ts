import TGBot from '../bot/bot.js'
import { getChatIds } from '../../models/chats/index.js'
import { isDayOff, isTimeToRelax } from '../../utils/datetime.js'

export async function notifyAboutEstimateSession() {
  const chatIds: Array<number> = await getChatIds()

  const body = `[⏰Самое время, чтобы оценить задачи](https://jira.stilsoft.ru/projects/S5MR?selectedItem=com.spartez.jira.plugins.jiraplanningpoker:pokerng-project-side-panel-link&rapidView=211&source=internal)`

  chatIds.forEach((chatId: number) => {
    if (chatId) {
      TGBot.sendEstimateSessionMessage(chatId, body)
      TGBot.getInstance().sendSticker(
        chatId,
        'https://cdn.tlgrm.ru/stickers/4ce/580/4ce58078-ce94-370f-8abb-836a56b5fa0b/256/2.webp'
      )
    }
  })
}

export function setEstimateSessionNotifier() {
  setInterval(async () => {
    const date = new Date()

    const hrs = date.getHours()
    const min = date.getMinutes()

    if (isDayOff() || isTimeToRelax()) return

    if (hrs === 9 && min <= 35 && min >= 25) {
      notifyAboutEstimateSession()
    }

    if (hrs === 12 && min <= 10 && min >= 0) {
      notifyAboutEstimateSession()
    }
  }, 660000)
}
