import { getMergeRequests } from '../../models/mergeRequest/index.js'
import { MergeRequestsState } from '../../controllers/gitHook/index.types.js'
import TGBot from '../bot/bot.js'
import { getChatIds } from '../../models/chats/index.js'
import { MergeRequestDBRow } from '../../models/mergeRequest/index.types.js'
import { isDayOff } from '../../utils/datetime.js'
import { logMessage } from '../../utils/logUtil.js'

export async function notifyAboutOpenedMergeRequests(
  singleChatId?: number,
  onMyOwn?: boolean
) {
  const response = await getMergeRequests()

  const openedMergeRequests = response.filter(
    (mr: MergeRequestDBRow) =>
      mr.state === MergeRequestsState.OPENED ||
      mr.state === MergeRequestsState.LOCKED
  )

  logMessage(
    'opened merge requests: ',
    openedMergeRequests.map((mr: MergeRequestDBRow) => ({ url: mr.mrurl }))
  )

  if (!openedMergeRequests.length && !singleChatId && !onMyOwn) return

  if (!singleChatId) {
    const chatIds: Array<number> = await getChatIds()

    chatIds.forEach((chatId: number) => {
      if (chatId) {
        TGBot.sendOpenedMergeRequestMessage(chatId, openedMergeRequests)
        TGBot.getInstance().sendSticker(
          chatId,
          'https://tlgrm.ru/_/stickers/67e/60c/67e60c3e-98b9-3cf5-8338-1c71364df6d2/10.webp'
        )
      }
    })

    return
  }

  TGBot.sendOpenedMergeRequestMessage(
    singleChatId,
    openedMergeRequests,
    onMyOwn
  )

  if (openedMergeRequests.length) {
    TGBot.getInstance().sendSticker(
      singleChatId,
      'https://tlgrm.ru/_/stickers/67e/60c/67e60c3e-98b9-3cf5-8338-1c71364df6d2/10.webp'
    )
  }
}

export function onOpenedMergeRequestsEvent(
  messages: MergeRequestDBRow[],
  onMyOwn: boolean = false
) {
  if (!messages.length && onMyOwn) {
    return 'У вас нет активных запросов на слияние'
  }

  const MILLISECONDS_IN_ONE_HR = 3_600_000

  const unapprovedMergeRequests = [
    `📬⏰ *В проекте есть открытые запросы на слияние:* \n`
  ]

  const sortedMessages = messages.reduce<Record<string, MergeRequestDBRow[]>>(
    (acc, cv) => {
      if (!acc[cv.projectname]?.length) {
        acc[cv.projectname] = [cv]
      } else {
        acc[cv.projectname] = [...acc[cv.projectname], cv]
      }

      return acc
    },
    {}
  )

  for (let [project, messages] of Object.entries(sortedMessages)) {
    unapprovedMergeRequests.push(
      `\nПроект: [${project}](${messages[0]?.projecturl})\n`
    )

    for (let message of messages) {
      const diffTime = new Date().getTime() - new Date(message.time).valueOf()
      const relativeHrs = diffTime / MILLISECONDS_IN_ONE_HR

      unapprovedMergeRequests.push(
        ` - [${message.sourcebranch} от ${message.username}](${
          message.mrurl
        }) (${relativeHrs.toFixed(1)} часов назад)🗿\n`
      )
    }
  }

  unapprovedMergeRequests.push('\n*Они ждут вашего ревью* ⏳')

  return unapprovedMergeRequests.join('')
}

export function setOpenedMrsTimer() {
  setInterval(async () => {
    const date = new Date()

    const hrs = date.getHours()
    const min = date.getMinutes()

    if (isDayOff()) return
    // logMessage('Interval check time:', { hrs, min, isDayOff: isDayOff() })

    if (hrs === 9 && min <= 35 && min >= 25) {
      notifyAboutOpenedMergeRequests()
      // logMessage(
      //   `Notifying about mr. isDayOff: ${isDayOff()}, hrs: ${hrs}, min: ${min}`
      // )
    }

    if (hrs === 14 && min <= 10 && min >= 0) {
      notifyAboutOpenedMergeRequests()
      // logMessage(
      //   `Notifying about mr. isDayOff: ${isDayOff()}, hrs: ${hrs}, min: ${min}`
      // )
    }
  }, 660000)
}
