const consoleStyles = {
  error:
    'background-color: #da5456; color: #000000; font-size: 16px; font-weight: 600;',
  warning:
    'background-color: #ffb703; color: #000000; font-size: 16px; font-weight: 600;',
  success:
    'background-color: #8cb369; color: #000000; font-size: 16px; font-weight: 600;',
  message:
    'background-color: #125ee4; color: #eff5ff; font-size: 16px; font-weight: 600;'
}

export const logError = console.log.bind(
  global.console,
  '%c Error: ',
  `${consoleStyles.error}`
)

export const logWarning = console.log.bind(
  global.console,
  '%c Warning: ',
  `${consoleStyles.warning}`
)

export const logSuccess = console.log.bind(
  global.console,
  '%c Success: ',
  `${consoleStyles.success}`
)

export const logMessage = (message?: unknown, ...optionalParams: unknown[]) => {
  console.log.bind(
    global.console,
    `%c [${new Date().toLocaleString()}]: `,
    `${consoleStyles.message}\n`
  )(message, ...optionalParams)
}
