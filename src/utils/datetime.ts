import { logMessage } from './logUtil.js'

export const isDayOff = () => {
  const date = new Date()
  // logMessage({ isDayOff: date.getDay() % 6 === 0, date })
  return date.getDay() % 6 === 0
}

export const isTimeToRelax = () => {
  const date = new Date()

  const hrs = date.getHours()
  const min = date.getMinutes()

  // logMessage({ isTimeToRelax: hrs > 19 || hrs < 7, hrs, min, date })
  return hrs > 19 || hrs < 7
}
