interface PushEvent {
  object_kind: string //'push',
  event_name: string //'push',
  before: string //'795c63d6d8909d7bf4565ae240ac92c8583f259d',
  after: string //'756ee08142dedd8810e1eec64efd3a244b4cb3f9',
  ref: string //'refs/heads/main',
  ref_protected: true
  checkout_sha: string //'756ee08142dedd8810e1eec64efd3a244b4cb3f9',
  message: any
  user_id: number
  user_name: string //'Artem Tatevosyan',
  user_username: string //'teqst',
  user_email: string //'',
  user_avatar: string //'https://secure.gravatar.com/avatar/911af84e72c4cfaf60c5843166ebc300f5e8b720d4b5eb935080758d5da916d0?s=80&d=identicon',
  project_id: number
  project: {
    id: number
    name: string //'stillab_bot',
    description: any
    web_url: string //'https://gitlab.com/teqst/stillab_bot',
    avatar_url: any
    git_ssh_url: string //'git@gitlab.com:teqst/stillab_bot.git',
    git_http_url: string //'https://gitlab.com/teqst/stillab_bot.git',
    namespace: string //'Artem Tatevosyan',
    visibility_level: number
    path_with_namespace: string //'teqst/stillab_bot',
    default_branch: string //'main',
    ci_config_path: string // '',
    homepage: string //'https://gitlab.com/teqst/stillab_bot',
    url: string //'git@gitlab.com:teqst/stillab_bot.git',
    ssh_url: string //'git@gitlab.com:teqst/stillab_bot.git',
    http_url: string //'https://gitlab.com/teqst/stillab_bot.git'
  }
  commits: {
    id: string //'756ee08142dedd8810e1eec64efd3a244b4cb3f9',
    message: string //'Поднята версия x2, исправлены скрипты для архивации и отправке на хост билда\n',
    title: string //'Поднята версия x2, исправлены скрипты для архивации и отправке на хост билда',
    timestamp: string //'2024-04-15T22:29:31+03:00',
    url: string //https://gitlab.com/teqst/stillab_bot/-/commit/756ee08142dedd8810e1eec64efd3a244b4cb3f9',
    author: { name: string; email: string }
    added: any[]
    modified: string[]
    removed: any[]
  }[]
  total_commits_count: 3
  push_options: {}
  repository: {
    name: string //'stillab_bot'
    url: string //'git@gitlab.com:teqst/stillab_bot.git'
    description: any
    homepage: string //'https://gitlab.com/teqst/stillab_bot'
    git_http_url: string //'https://gitlab.com/teqst/stillab_bot.git'
    git_ssh_url: string //'git@gitlab.com:teqst/stillab_bot.git'
    visibility_level: number
  }
}

const pushEventMock = {
  object_kind: 'push',
  event_name: 'push',
  before: '795c63d6d8909d7bf4565ae240ac92c8583f259d',
  after: '756ee08142dedd8810e1eec64efd3a244b4cb3f9',
  ref: 'refs/heads/main',
  ref_protected: true,
  checkout_sha: '756ee08142dedd8810e1eec64efd3a244b4cb3f9',
  message: null,
  user_id: 7861727,
  user_name: 'Artem Tatevosyan',
  user_username: 'teqst',
  user_email: '',
  user_avatar:
    'https://secure.gravatar.com/avatar/911af84e72c4cfaf60c5843166ebc300f5e8b720d4b5eb935080758d5da916d0?s=80&d=identicon',
  project_id: 47560921,
  project: {
    id: 47560921,
    name: 'stillab_bot',
    description: null,
    web_url: 'https://gitlab.com/teqst/stillab_bot',
    avatar_url: null,
    git_ssh_url: 'git@gitlab.com:teqst/stillab_bot.git',
    git_http_url: 'https://gitlab.com/teqst/stillab_bot.git',
    namespace: 'Artem Tatevosyan',
    visibility_level: 20,
    path_with_namespace: 'teqst/stillab_bot',
    default_branch: 'main',
    ci_config_path: '',
    homepage: 'https://gitlab.com/teqst/stillab_bot',
    url: 'git@gitlab.com:teqst/stillab_bot.git',
    ssh_url: 'git@gitlab.com:teqst/stillab_bot.git',
    http_url: 'https://gitlab.com/teqst/stillab_bot.git'
  },
  commits: [
    {
      id: '756ee08142dedd8810e1eec64efd3a244b4cb3f9',
      message:
        'Поднята версия x2, исправлены скрипты для архивации и отправке на хост билда\n',
      title:
        'Поднята версия x2, исправлены скрипты для архивации и отправке на хост билда',
      timestamp: '2024-04-15T22:29:31+03:00',
      url: 'https://gitlab.com/teqst/stillab_bot/-/commit/756ee08142dedd8810e1eec64efd3a244b4cb3f9',
      author: { name: 'temaqwest_m', email: 'taylorartem95@gmail.com' },
      added: [],
      modified: ['.env', 'package.json'],
      removed: []
    },
    {
      id: '42f8749085a493fe37dfe4d70cbbb8c1d7427743',
      message:
        'Поднята версия, добавлены скрипты для архивации и отправке на хост билда\n',
      title:
        'Поднята версия, добавлены скрипты для архивации и отправке на хост билда',
      timestamp: '2024-04-15T22:21:37+03:00',
      url: 'https://gitlab.com/teqst/stillab_bot/-/commit/42f8749085a493fe37dfe4d70cbbb8c1d7427743',
      author: { name: 'temaqwest_m', email: 'taylorartem95@gmail.com' },
      added: [],
      modified: ['package.json'],
      removed: []
    },
    {
      id: '795c63d6d8909d7bf4565ae240ac92c8583f259d',
      message:
        'Добавлено ограничение по времени для отправки оповещений о мрах, ноутах, пушах, работе пайплайна в чаты участников\n',
      title:
        'Добавлено ограничение по времени для отправки оповещений о мрах, ноутах,...',
      timestamp: '2024-04-15T22:18:04+03:00',
      url: 'https://gitlab.com/teqst/stillab_bot/-/commit/795c63d6d8909d7bf4565ae240ac92c8583f259d',
      author: { name: 'temaqwest_m', email: 'taylorartem95@gmail.com' },
      added: ['src/utils/datetime.ts'],
      modified: [
        'src/controllers/gitHook/index.ts',
        'src/entities/notifier/mergeRequestNotifier.ts'
      ],
      removed: []
    }
  ],
  total_commits_count: 3,
  push_options: {},
  repository: {
    name: 'stillab_bot',
    url: 'git@gitlab.com:teqst/stillab_bot.git',
    description: null,
    homepage: 'https://gitlab.com/teqst/stillab_bot',
    git_http_url: 'https://gitlab.com/teqst/stillab_bot.git',
    git_ssh_url: 'git@gitlab.com:teqst/stillab_bot.git',
    visibility_level: 20
  }
}

const pipelineEventMock = {
  object_kind: 'pipeline',
  object_attributes: {
    id: 1253937429,
    iid: 29,
    name: null,
    ref: 'main',
    tag: false,
    sha: '756ee08142dedd8810e1eec64efd3a244b4cb3f9',
    before_sha: '42f8749085a493fe37dfe4d70cbbb8c1d7427743',
    source: 'push',
    status: 'success',
    detailed_status: 'passed',
    stages: ['build', 'test', 'deploy'],
    created_at: '2024-04-15T19:29:37.140Z',
    finished_at: '2024-04-15T19:32:05.055Z',
    duration: 145,
    queued_duration: 1,
    variables: [],
    url: 'https://gitlab.com/teqst/stillab_bot/-/pipelines/1253937429'
  },
  merge_request: null,
  user: {
    id: 7861727,
    name: 'Artem Tatevosyan',
    username: 'teqst',
    avatar_url:
      'https://secure.gravatar.com/avatar/911af84e72c4cfaf60c5843166ebc300f5e8b720d4b5eb935080758d5da916d0?s=80&d=identicon',
    email: '[REDACTED]'
  },
  project: {
    id: 47560921,
    name: 'stillab_bot',
    description: null,
    web_url: 'https://gitlab.com/teqst/stillab_bot',
    avatar_url: null,
    git_ssh_url: 'git@gitlab.com:teqst/stillab_bot.git',
    git_http_url: 'https://gitlab.com/teqst/stillab_bot.git',
    namespace: 'Artem Tatevosyan',
    visibility_level: 20,
    path_with_namespace: 'teqst/stillab_bot',
    default_branch: 'main',
    ci_config_path: ''
  },
  commit: {
    id: '756ee08142dedd8810e1eec64efd3a244b4cb3f9',
    message:
      'Поднята версия x2, исправлены скрипты для архивации и отправке на хост билда\n',
    title:
      'Поднята версия x2, исправлены скрипты для архивации и отправке на хост билда',
    timestamp: '2024-04-15T22:29:31+03:00',
    url: 'https://gitlab.com/teqst/stillab_bot/-/commit/756ee08142dedd8810e1eec64efd3a244b4cb3f9',
    author: { name: 'temaqwest_m', email: 'taylorartem95@gmail.com' }
  },
  builds: [
    {
      id: 6630790456,
      stage: 'test',
      name: 'lint-test-job',
      status: 'success',
      created_at: '2024-04-15T19:29:37.213Z',
      started_at: '2024-04-15T19:30:06.678Z',
      finished_at: '2024-04-15T19:30:46.599Z',
      duration: 39.921214,
      queued_duration: 0.260469,
      failure_reason: null,
      when: 'on_success',
      manual: false,
      allow_failure: false,
      user: {
        id: 7861727,
        name: 'Artem Tatevosyan',
        username: 'teqst',
        avatar_url:
          'https://secure.gravatar.com/avatar/911af84e72c4cfaf60c5843166ebc300f5e8b720d4b5eb935080758d5da916d0?s=80\u0026d=identicon',
        email: '[REDACTED]'
      },
      runner: {
        id: 12270831,
        description:
          '2-blue.saas-linux-small-amd64.runners-manager.gitlab.com/default',
        runner_type: 'instance_type',
        active: true,
        is_shared: true,
        tags: [
          'gce',
          'east-c',
          'linux',
          'ruby',
          'mysql',
          'postgres',
          'mongo',
          'git-annex',
          'shared',
          'docker',
          'saas-linux-small-amd64'
        ]
      },
      artifacts_file: { filename: null, size: null },
      environment: null
    },
    {
      id: 6630790455,
      stage: 'test',
      name: 'unit-test-job',
      status: 'success',
      created_at: '2024-04-15T19:29:37.205Z',
      started_at: '2024-04-15T19:30:06.623Z',
      finished_at: '2024-04-15T19:31:34.203Z',
      duration: 87.579317,
      queued_duration: 0.278944,
      failure_reason: null,
      when: 'on_success',
      manual: false,
      allow_failure: false,
      user: {
        id: 7861727,
        name: 'Artem Tatevosyan',
        username: 'teqst',
        avatar_url:
          'https://secure.gravatar.com/avatar/911af84e72c4cfaf60c5843166ebc300f5e8b720d4b5eb935080758d5da916d0?s=80\u0026d=identicon',
        email: '[REDACTED]'
      },
      runner: {
        id: 12270831,
        description:
          '2-blue.saas-linux-small-amd64.runners-manager.gitlab.com/default',
        runner_type: 'instance_type',
        active: true,
        is_shared: true,
        tags: [
          'gce',
          'east-c',
          'linux',
          'ruby',
          'mysql',
          'postgres',
          'mongo',
          'git-annex',
          'shared',
          'docker',
          'saas-linux-small-amd64'
        ]
      },
      artifacts_file: { filename: null, size: null },
      environment: null
    },
    {
      id: 6630790457,
      stage: 'deploy',
      name: 'deploy-job',
      status: 'success',
      created_at: '2024-04-15T19:29:37.228Z',
      started_at: '2024-04-15T19:31:34.706Z',
      finished_at: '2024-04-15T19:32:04.864Z',
      duration: 30.15729,
      queued_duration: 0.144554,
      failure_reason: null,
      when: 'on_success',
      manual: false,
      allow_failure: false,
      user: {
        id: 7861727,
        name: 'Artem Tatevosyan',
        username: 'teqst',
        avatar_url:
          'https://secure.gravatar.com/avatar/911af84e72c4cfaf60c5843166ebc300f5e8b720d4b5eb935080758d5da916d0?s=80\u0026d=identicon',
        email: '[REDACTED]'
      },
      runner: {
        id: 12270831,
        description:
          '2-blue.saas-linux-small-amd64.runners-manager.gitlab.com/default',
        runner_type: 'instance_type',
        active: true,
        is_shared: true,
        tags: [
          'gce',
          'east-c',
          'linux',
          'ruby',
          'mysql',
          'postgres',
          'mongo',
          'git-annex',
          'shared',
          'docker',
          'saas-linux-small-amd64'
        ]
      },
      artifacts_file: { filename: null, size: null },
      environment: {
        name: 'production',
        action: 'start',
        deployment_tier: 'production'
      }
    },
    {
      id: 6630790453,
      stage: 'build',
      name: 'build-job',
      status: 'success',
      created_at: '2024-04-15T19:29:37.172Z',
      started_at: '2024-04-15T19:29:38.089Z',
      finished_at: '2024-04-15T19:30:06.231Z',
      duration: 28.141216,
      queued_duration: 0.187131,
      failure_reason: null,
      when: 'on_success',
      manual: false,
      allow_failure: false,
      user: {
        id: 7861727,
        name: 'Artem Tatevosyan',
        username: 'teqst',
        avatar_url:
          'https://secure.gravatar.com/avatar/911af84e72c4cfaf60c5843166ebc300f5e8b720d4b5eb935080758d5da916d0?s=80\u0026d=identicon',
        email: '[REDACTED]'
      },
      runner: {
        id: 32976687,
        description:
          '6-blue.saas-linux-small-amd64.runners-manager.gitlab.com/default',
        runner_type: 'instance_type',
        active: true,
        is_shared: true,
        tags: [
          'docker',
          'east-c',
          'git-annex',
          'linux',
          'ruby',
          'shared',
          'mongo',
          'mysql',
          'gce',
          'postgres',
          'saas-linux-small-amd64'
        ]
      },
      artifacts_file: { filename: null, size: null },
      environment: null
    }
  ]
}

const noteEventMock = {
  object_kind: 'note',
  event_type: 'note',
  user: {
    id: 7861727,
    name: 'Artem Tatevosyan',
    username: 'teqst',
    avatar_url:
      'https://secure.gravatar.com/avatar/911af84e72c4cfaf60c5843166ebc300f5e8b720d4b5eb935080758d5da916d0?s=80&d=identicon',
    email: '[REDACTED]'
  },
  project_id: 47560921,
  project: {
    id: 47560921,
    name: 'stillab_bot',
    description: null,
    web_url: 'https://gitlab.com/teqst/stillab_bot',
    avatar_url: null,
    git_ssh_url: 'git@gitlab.com:teqst/stillab_bot.git',
    git_http_url: 'https://gitlab.com/teqst/stillab_bot.git',
    namespace: 'Artem Tatevosyan',
    visibility_level: 20,
    path_with_namespace: 'teqst/stillab_bot',
    default_branch: 'main',
    ci_config_path: '',
    homepage: 'https://gitlab.com/teqst/stillab_bot',
    url: 'git@gitlab.com:teqst/stillab_bot.git',
    ssh_url: 'git@gitlab.com:teqst/stillab_bot.git',
    http_url: 'https://gitlab.com/teqst/stillab_bot.git'
  },
  object_attributes: {
    attachment: null,
    author_id: 7861727,
    change_position: null,
    commit_id: null,
    created_at: '2023-07-25T20:48:28.646Z',
    discussion_id: '1e41484f7ba0473f5ca581eed109bf470c22c63b',
    id: 1487913263,
    line_code: null,
    note: 'added 1 commit\n\n<ul><li>a88e0ecf - env changes</li></ul>\n\n[Compare with previous version](/teqst/stillab_bot/-/merge_requests/3/diffs?diff_id=744183131&start_sha=ef45101570ab2cfbf3dcba9be7293a065c2cd677)',
    noteable_id: 239356490,
    noteable_type: 'MergeRequest',
    original_position: null,
    position: null,
    project_id: 47560921,
    resolved_at: null,
    resolved_by_id: null,
    resolved_by_push: null,
    st_diff: null,
    system: true,
    type: null,
    updated_at: '2023-07-25T20:48:28.648Z',
    updated_by_id: null,
    description:
      'added 1 commit\n\n<ul><li>a88e0ecf - env changes</li></ul>\n\n[Compare with previous version](/teqst/stillab_bot/-/merge_requests/3/diffs?diff_id=744183131&start_sha=ef45101570ab2cfbf3dcba9be7293a065c2cd677)',
    url: 'https://gitlab.com/teqst/stillab_bot/-/merge_requests/3#note_1487913263',
    action: 'create'
  },
  repository: {
    name: 'stillab_bot',
    url: 'git@gitlab.com:teqst/stillab_bot.git',
    description: null,
    homepage: 'https://gitlab.com/teqst/stillab_bot'
  },
  merge_request: {
    assignee_id: null,
    author_id: 7861727,
    created_at: '2023-07-25T20:42:26.645Z',
    description: 'slav',
    draft: false,
    head_pipeline_id: 945252162,
    id: 239356490,
    iid: 3,
    last_edited_at: null,
    last_edited_by_id: null,
    merge_commit_sha: 'a88e0ecff16dc378a952140f6b06e9ef8f96b559',
    merge_error: null,
    merge_params: { force_remove_source_branch: '1' },
    merge_status: 'can_be_merged',
    merge_user_id: null,
    merge_when_pipeline_succeeds: false,
    milestone_id: null,
    source_branch: 'feature-teqst',
    source_project_id: 47560921,
    state_id: 3,
    target_branch: 'main',
    target_project_id: 47560921,
    time_estimate: 0,
    title: 'Feature teqst',
    updated_at: '2023-07-25T20:51:40.736Z',
    updated_by_id: null,
    prepared_at: '2023-07-25T20:42:28.679Z',
    url: 'https://gitlab.com/teqst/stillab_bot/-/merge_requests/3',
    source: {
      id: 47560921,
      name: 'stillab_bot',
      description: null,
      web_url: 'https://gitlab.com/teqst/stillab_bot',
      avatar_url: null,
      git_ssh_url: 'git@gitlab.com:teqst/stillab_bot.git',
      git_http_url: 'https://gitlab.com/teqst/stillab_bot.git',
      namespace: 'Artem Tatevosyan',
      visibility_level: 20,
      path_with_namespace: 'teqst/stillab_bot',
      default_branch: 'main',
      ci_config_path: '',
      homepage: 'https://gitlab.com/teqst/stillab_bot',
      url: 'git@gitlab.com:teqst/stillab_bot.git',
      ssh_url: 'git@gitlab.com:teqst/stillab_bot.git',
      http_url: 'https://gitlab.com/teqst/stillab_bot.git'
    },
    target: {
      id: 47560921,
      name: 'stillab_bot',
      description: null,
      web_url: 'https://gitlab.com/teqst/stillab_bot',
      avatar_url: null,
      git_ssh_url: 'git@gitlab.com:teqst/stillab_bot.git',
      git_http_url: 'https://gitlab.com/teqst/stillab_bot.git',
      namespace: 'Artem Tatevosyan',
      visibility_level: 20,
      path_with_namespace: 'teqst/stillab_bot',
      default_branch: 'main',
      ci_config_path: '',
      homepage: 'https://gitlab.com/teqst/stillab_bot',
      url: 'git@gitlab.com:teqst/stillab_bot.git',
      ssh_url: 'git@gitlab.com:teqst/stillab_bot.git',
      http_url: 'https://gitlab.com/teqst/stillab_bot.git'
    },
    last_commit: {
      id: 'a88e0ecff16dc378a952140f6b06e9ef8f96b559',
      message: 'env changes\n',
      title: 'env changes',
      timestamp: '2023-07-25T23:48:20+03:00',
      url: 'https://gitlab.com/teqst/stillab_bot/-/commit/a88e0ecff16dc378a952140f6b06e9ef8f96b559',
      author: { name: 'Artem Tatevosyan', email: 'taylorartem95@gmail.com' }
    },
    work_in_progress: false,
    total_time_spent: 0,
    time_change: 0,
    human_total_time_spent: null,
    human_time_change: null,
    human_time_estimate: null,
    assignee_ids: [],
    reviewer_ids: [],
    labels: [],
    state: 'merged',
    blocking_discussions_resolved: true,
    first_contribution: false,
    detailed_merge_status: 'not_open'
  }
}

const mergeRequestEventMock = {
  object_kind: 'merge_request',
  event_type: 'merge_request',
  user: {
    id: 7861727,
    name: 'Artem Tatevosyan',
    username: 'teqst',
    avatar_url:
      'https://secure.gravatar.com/avatar/911af84e72c4cfaf60c5843166ebc300f5e8b720d4b5eb935080758d5da916d0?s=80&d=identicon',
    email: '[REDACTED]'
  },
  project: {
    id: 47560921,
    name: 'stillab_bot',
    description: null,
    web_url: 'https://gitlab.com/teqst/stillab_bot',
    avatar_url: null,
    git_ssh_url: 'git@gitlab.com:teqst/stillab_bot.git',
    git_http_url: 'https://gitlab.com/teqst/stillab_bot.git',
    namespace: 'Artem Tatevosyan',
    visibility_level: 20,
    path_with_namespace: 'teqst/stillab_bot',
    default_branch: 'main',
    ci_config_path: '',
    homepage: 'https://gitlab.com/teqst/stillab_bot',
    url: 'git@gitlab.com:teqst/stillab_bot.git',
    ssh_url: 'git@gitlab.com:teqst/stillab_bot.git',
    http_url: 'https://gitlab.com/teqst/stillab_bot.git'
  },
  object_attributes: {
    assignee_id: null,
    author_id: 7861727,
    created_at: '2023-07-25T20:42:26.645Z',
    description: 'slav',
    draft: false,
    head_pipeline_id: 945252162,
    id: 239356490,
    iid: 3,
    last_edited_at: null,
    last_edited_by_id: null,
    merge_commit_sha: 'a88e0ecff16dc378a952140f6b06e9ef8f96b559',
    merge_error: null,
    merge_params: { force_remove_source_branch: '1' },
    merge_status: 'can_be_merged',
    merge_user_id: null,
    merge_when_pipeline_succeeds: false,
    milestone_id: null,
    source_branch: 'feature-teqst',
    source_project_id: 47560921,
    state_id: 3,
    target_branch: 'main',
    target_project_id: 47560921,
    time_estimate: 0,
    title: 'Feature teqst',
    updated_at: '2023-07-25T20:51:40.736Z',
    updated_by_id: null,
    prepared_at: '2023-07-25T20:42:28.679Z',
    url: 'https://gitlab.com/teqst/stillab_bot/-/merge_requests/3',
    source: {
      id: 47560921,
      name: 'stillab_bot',
      description: null,
      web_url: 'https://gitlab.com/teqst/stillab_bot',
      avatar_url: null,
      git_ssh_url: 'git@gitlab.com:teqst/stillab_bot.git',
      git_http_url: 'https://gitlab.com/teqst/stillab_bot.git',
      namespace: 'Artem Tatevosyan',
      visibility_level: 20,
      path_with_namespace: 'teqst/stillab_bot',
      default_branch: 'main',
      ci_config_path: '',
      homepage: 'https://gitlab.com/teqst/stillab_bot',
      url: 'git@gitlab.com:teqst/stillab_bot.git',
      ssh_url: 'git@gitlab.com:teqst/stillab_bot.git',
      http_url: 'https://gitlab.com/teqst/stillab_bot.git'
    },
    target: {
      id: 47560921,
      name: 'stillab_bot',
      description: null,
      web_url: 'https://gitlab.com/teqst/stillab_bot',
      avatar_url: null,
      git_ssh_url: 'git@gitlab.com:teqst/stillab_bot.git',
      git_http_url: 'https://gitlab.com/teqst/stillab_bot.git',
      namespace: 'Artem Tatevosyan',
      visibility_level: 20,
      path_with_namespace: 'teqst/stillab_bot',
      default_branch: 'main',
      ci_config_path: '',
      homepage: 'https://gitlab.com/teqst/stillab_bot',
      url: 'git@gitlab.com:teqst/stillab_bot.git',
      ssh_url: 'git@gitlab.com:teqst/stillab_bot.git',
      http_url: 'https://gitlab.com/teqst/stillab_bot.git'
    },
    last_commit: {
      id: 'a88e0ecff16dc378a952140f6b06e9ef8f96b559',
      message: 'env changes\n',
      title: 'env changes',
      timestamp: '2023-07-25T23:48:20+03:00',
      url: 'https://gitlab.com/teqst/stillab_bot/-/commit/a88e0ecff16dc378a952140f6b06e9ef8f96b559',
      author: { name: 'Artem Tatevosyan', email: 'taylorartem95@gmail.com' }
    },
    work_in_progress: false,
    total_time_spent: 0,
    time_change: 0,
    human_total_time_spent: null,
    human_time_change: null,
    human_time_estimate: null,
    assignee_ids: [],
    reviewer_ids: [],
    labels: [],
    state: 'merged',
    blocking_discussions_resolved: true,
    first_contribution: false,
    detailed_merge_status: 'not_open'
  },
  labels: [],
  changes: {},
  repository: {
    name: 'stillab_bot',
    url: 'git@gitlab.com:teqst/stillab_bot.git',
    description: null,
    homepage: 'https://gitlab.com/teqst/stillab_bot'
  }
}
