export type Config = {
  DB_NAME: string
  DB_HOST: string
  DB_PORT: number
  DB_USER: string
  DB_PASS: string
  TOKEN: string
}

export const IS_DEV = Boolean(Number(process.env.IS_DEV))

export const CONFIG: Config = {
  DB_NAME: process.env.DB_NAME ?? '',
  DB_HOST: (IS_DEV ? process.env.DEV_DB_HOST : process.env.DB_HOST) ?? '',
  DB_PORT: Number(process.env.DB_PORT) ?? 5432,
  DB_USER: process.env.DB_USER ?? '',
  DB_PASS: process.env.DB_PASS ?? '',
  TOKEN: (IS_DEV ? process.env.DEV_TOKEN : process.env.TOKEN) ?? ''
}
