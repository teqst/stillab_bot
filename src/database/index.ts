import pkg, { QueryConfig } from 'pg'
import { CONFIG } from '../config.js'
const { Client } = pkg

export async function dbQuery(query: string | QueryConfig) {
  try {
    const client = new Client({
      database: CONFIG.DB_NAME,
      host: CONFIG.DB_HOST,
      port: CONFIG.DB_PORT,
      user: CONFIG.DB_USER,
      password: CONFIG.DB_PASS
    })

    await client.connect()
    const res = await client.query(query)
    client.end()

    return res.rows
  } catch (err) {
    console.error('Problem executing query: ')
    console.error(err)
    throw err
  }
}
