import { dbQuery } from '../../database/index.js'
import { MergeRequestEventObject } from '../../controllers/gitHook/index.types.js'
import { logMessage } from '../../utils/logUtil.js'

async function initMergeTable() {
  await dbQuery(
    `CREATE TABLE IF NOT EXISTS mergerequests 
        (
            id SERIAL PRIMARY KEY,
            mrId INT NOT NULL,
            time TIMESTAMP,
            username VARCHAR(100),
            mrUrl TEXT,
            projectUrl TEXT,
            sourceBranch VARCHAR(80),
            targetBranch VARCHAR(80),
            projectName VARCHAR(250),
            state VARCHAR(20)
        );`
  )
}

export async function saveMergeRequest(mrObject: MergeRequestEventObject) {
  await dbQuery("SET TIMEZONE TO 'Utc/GMT-3';")

  const query = {
    text: `INSERT INTO mergerequests (mrId, time, username, mrUrl, projectUrl, sourceBranch, targetBranch, projectName, state)
            VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9)`,
    values: [
      mrObject.object_attributes.id,
      'NOW()',
      mrObject.user.username,
      mrObject.object_attributes.url,
      mrObject.project.web_url,
      mrObject.object_attributes.source_branch,
      mrObject.object_attributes.target_branch,
      mrObject.project.name,
      mrObject.object_attributes.state
    ]
  }

  const { object_attributes, object_kind } = mrObject

  logMessage('saveMergeRequest: ', {
    object_kind,
    createdAt: object_attributes.created_at,
    updated_at: object_attributes.updated_at,
    iid: object_attributes.iid,
    status: object_attributes.merge_status,
    state: object_attributes.state,
    action: object_attributes.action
  })

  await dbQuery(query)
}

export function getMergeRequests() {
  return dbQuery('SELECT * FROM mergerequests')
}

export async function updateMrStatus(mrObject: MergeRequestEventObject) {
  const { object_attributes, object_kind } = mrObject

  logMessage('updateMrStatus: ', {
    object_kind,
    createdAt: object_attributes.created_at,
    updated_at: object_attributes.updated_at,
    iid: object_attributes.iid,
    status: object_attributes.merge_status,
    state: object_attributes.state,
    action: object_attributes.action
  })

  await dbQuery(
    `UPDATE mergerequests SET state='${mrObject.object_attributes.state}' WHERE mrId=${mrObject.object_attributes.id};`
  )
}

export async function isMergeRequestExists(mrObject: MergeRequestEventObject) {
  const response = await dbQuery(
    `SELECT * FROM mergerequests WHERE mrId = ${mrObject.object_attributes.id};`
  )
  logMessage(
    `is Merge Request (${mrObject.object_attributes.id})(${
      mrObject.object_attributes.iid
    }) Exists = ${!!response.length}`,
    response
  )

  return !!response.length
}

export async function writeMrData(mrObject: MergeRequestEventObject) {
  await initMergeTable()

  const isExists = await isMergeRequestExists(mrObject)

  if (isExists) {
    await updateMrStatus(mrObject)
  } else {
    await saveMergeRequest(mrObject)
  }
}
