export interface MergeRequestDBRow {
  id: number
  mrid: number
  time: string
  username: string
  mrurl: string
  projecturl: string
  sourcebranch: string
  targetbranch: string
  projectname: string
  state: string
}
