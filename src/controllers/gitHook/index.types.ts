export enum GitlabEventKind {
  PUSH = 'push',
  MERGE_REQUEST = 'merge_request',
  PIPELINE = 'pipeline',
  NOTE = 'note'
}

export interface GitlabBaseHookReq {
  object_kind: GitlabEventKind
  project: {
    default_branch: string
    git_http_url: string
    git_ssh_url: string
    homepage: string
    http_url: string
    id: number
    name: string
    namespace: string
    web_url: string
  }
}

export interface PipelineEventObject extends GitlabBaseHookReq {
  object_kind: GitlabEventKind.PIPELINE
  object_attributes: {
    id: number
    ref: string
    status: string
    detailed_status: string
  }
}

export enum PipelineStatuses {
  failed = 'failed',
  success = 'success'
}

export enum MergeRequestsState {
  CLOSED = 'closed',
  MERGED = 'merged',
  OPENED = 'opened',
  LOCKED = 'locked'
}

export enum MergeRequestActions {
  open = 'open',
  close = 'close',
  reopen = 'reopen',
  update = 'update',
  approved = 'approved',
  unapproved = 'unapproved',
  approval = 'approval',
  unapproval = 'unapproval',
  merge = 'merge'
}

export interface MergeRequestEventObject extends GitlabBaseHookReq {
  object_kind: GitlabEventKind.MERGE_REQUEST
  user: {
    name: string
    username: string
    avatar_url: string
  }
  object_attributes: {
    id: number
    merge_error: boolean | null | string
    merge_status: string
    source_branch: string
    target_branch: string
    url: string
    state: MergeRequestsState
    action: MergeRequestActions
    created_at: string
    iid: number
    updated_at: string
    prepared_at: string
  }
}
export interface PushEventObject extends GitlabBaseHookReq {
  object_kind: GitlabEventKind.PUSH
  user_name: string
  user_username: string
  ref: string
  commits: [
    {
      url: string
    }
  ]
}

export interface NoteEventObject extends GitlabBaseHookReq {
  object_kind: GitlabEventKind.NOTE
  user: {
    name: string
    username: string
    avatar_url: string
  }
  object_attributes: {
    url: string
    note: string
    description: string
  }
}
