import { Request, Response } from 'express'
import {
  GitlabBaseHookReq,
  GitlabEventKind,
  MergeRequestEventObject,
  NoteEventObject,
  PipelineEventObject,
  PushEventObject
} from './index.types.js'
import { getChatIds } from '../../models/chats/index.js'
import { writeMrData } from '../../models/mergeRequest/index.js'
import TGBot from '../../entities/bot/bot.js'
import { isTimeToRelax } from '../../utils/datetime.js'
import { logMessage } from '../../utils/logUtil.js'

export async function gitHookController(request: Request, response: Response) {
  const baseBody: GitlabBaseHookReq = request?.body
  const objectKind = baseBody?.object_kind

  if (!objectKind) response.end()

  const chatIds: Array<number> = await getChatIds()

  if (objectKind === GitlabEventKind.MERGE_REQUEST) {
    const eventBody = baseBody as MergeRequestEventObject
    await writeMrData(eventBody)

    chatIds.forEach((chatId: number) => {
      if (chatId && !isTimeToRelax()) {
        TGBot.sendMergeRequestMessage(chatId, eventBody)
      }
    })
  }

  if (objectKind === GitlabEventKind.PIPELINE) {
    const eventBody = baseBody as PipelineEventObject

    chatIds.forEach((chatId: number) => {
      if (chatId && !isTimeToRelax()) {
        TGBot.sendPipelineMessage(chatId, eventBody)
      }
    })
  }

  if (objectKind === GitlabEventKind.PUSH) {
    const eventBody = baseBody as PushEventObject

    chatIds.forEach((chatId: number) => {
      if (chatId && !isTimeToRelax()) {
        TGBot.sendPushMessage(chatId, eventBody)
      }
    })
  }

  if (objectKind === GitlabEventKind.NOTE) {
    const eventBody = baseBody as NoteEventObject

    chatIds.forEach((chatId: number) => {
      if (chatId && !isTimeToRelax()) {
        TGBot.sendNoteMessage(chatId, eventBody)
      }
    })
  }

  response.end()
}
