import express from 'express'
import { gitHookController } from '../../controllers/gitHook/index.js'

const router = express.Router()

router.post('/', gitHookController)

export default router
